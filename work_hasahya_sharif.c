#include<stdio.h>
#include<math.h>

int main(){
    float a,b,c,d,r1,r2,real,img;
    printf("Enter the coefficient of x^2\n");
    scanf("%f",&a);
    while (a==0){
    printf("Enter the coefficient of x^2\n");
    scanf("%f",&a);	
	}
    printf("Enter the coefficient of x\n");
    scanf("%f",&b);
    printf("Enter the value of the constant\n");
    scanf("%f",&c);
// Calculating for the discriminant value d
    d=(b*b)-(4*a*c);
//Condition for no real roots (immaginary roots) when d is less than zero    
	if(d<0){
    	printf("The roots are compex\n");
		real=(-b/(2*a));
		img=((sqrt(-d))/(2*a));
		printf("The roots of the quadratic equation (%.2fx^2 + %.2fx + %.2f) are: %.2f + %.2fi and %.2f - %.2fi;",a,b,c,real, img, real, img);
            	
	}
// Condition for real distinct roots that is when d is greater than zero
	else if(d>0){
		r1=(-b+sqrt(d))/(2*a);
    	r2=(-b-sqrt(d))/(2*a);
    	printf("The roots of the quadratic equation (%.2fx^2 + %.2fx + %.2f)  are: %.2f and %.2f\n",a,b,c,r1,r2);
    }
// Condition for repeated roots that is when d is equal to zero
    else if(d==0){
    	printf("The roots are the same\n");
    	r1=(-b+sqrt(d))/(2*a);
    	r2=(-b-sqrt(d))/(2*a);
    	printf("The roots of the quadratic equation (%.2fx^2+ %.2fx+ %.2f)  are: %.2f and %.2f\n",a,b,c,r1,r2);
    	
	}
   	return 0;		
}
